package mi.vmm.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mi.vmm.entity.FeatureSignature;
import mi.vmm.entity.Tuple;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Scalar;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.features2d.KeyPoint;
import org.opencv.highgui.Highgui;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class FeatureSignatureDAO {
    private static int MAX_KEYPOINTS = 500;
    private static final int MAX_KEYPOINT_SIZE = 10;
    
    public static void setMaxKeypoints(int maxKeypoints) {
        MAX_KEYPOINTS = maxKeypoints;
    }
    
    public static FeatureSignature getFeatureSignature(String fileName) {
        FeatureSignature fs = loadFromFile(fileName);
        if(fs == null) {
            fs = computeSignature(fileName);
            saveToFile(fs, fileName);
        }
        return fs;
    }
    
    private static FeatureSignature loadFromFile(String fileName) {       
        File f = new File("feature_signatures/" + fileName + ".fs");
        if(f.isFile()) { 
            try(BufferedReader in = new BufferedReader(new FileReader(f))) {
                FeatureSignature fs = new FeatureSignature();
                String line = "";
                while((line = in.readLine()) != null) {
                    Mat centroid = new Mat(1, 128, CvType.CV_64F);
                    String[] numbers = line.substring(0, line.indexOf(':')).split(",");
                    for (int i = 0; i < numbers.length; i++) {
                        centroid.put(0, i, Double.parseDouble(numbers[i]));
                    }
                    double weight = Double.parseDouble(line.substring(line.indexOf(':') + 1));
                    fs.addTuple(new Tuple(centroid, weight));
                }
                return fs;
            } catch (Exception ex) {
                Logger.getLogger(FeatureSignatureDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    private static FeatureSignature computeSignature(String fileName) {       
        System.out.println("compute signature for "+fileName);
        Mat img = Highgui.imread("img/" + fileName);
        
        // detect keypoints
        FeatureDetector detector = FeatureDetector.create(FeatureDetector.SIFT);
        MatOfKeyPoint keypoints = new MatOfKeyPoint();
        detector.detect(img, keypoints);
        
        // reduce number of keypoints
        KeyPoint[] all = keypoints.toArray();
        List<KeyPoint> selected = new ArrayList<>();
        
        // select edge-like keypoints
        for(int i = 0; i < all.length; i++) {
            KeyPoint kp = all[i];
            if(kp.size < MAX_KEYPOINT_SIZE) {
                selected.add(kp);
            }
        }
        
        // sort keypoints by response desc
        Collections.sort(selected, new Comparator<KeyPoint>() {
            @Override
            public int compare(KeyPoint kp1, KeyPoint kp2) {
                if(kp2.response > kp1.response) {
                    return 1;
                }
                else if (kp2.response < kp1.response) {
                    return -1;
                }
                return 0;
            }
        });
        
        keypoints = new MatOfKeyPoint();
        keypoints.fromList(selected.subList(0, selected.size() > MAX_KEYPOINTS ? MAX_KEYPOINTS - 1 : selected.size() - 1));
        
        // save image with keypoints
        Mat outImg = new Mat();  
        Features2d.drawKeypoints(img, keypoints, outImg, Scalar.all(-1), Features2d.DRAW_RICH_KEYPOINTS);
        Highgui.imwrite("keypoints/" + fileName + ".jpg", outImg);
        
        // extract descriptors
        DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.SIFT);
        Mat descriptors = new Mat();
        extractor.compute(img, keypoints, descriptors);
        
        // calculate total weight
        KeyPoint[] kpa = keypoints.toArray();
        double totalWeight = 0;
        for (KeyPoint keyPoint : kpa) {
            totalWeight += keyPoint.response;
        }
        
        // create feature signature
        FeatureSignature fs = new FeatureSignature();
        for (int i = 0; i < descriptors.rows(); i++) {        
            fs.addTuple(new Tuple(descriptors.row(i), kpa[i].response  / totalWeight));
        }
        return fs;
    }
    
    private static void saveToFile(FeatureSignature fs, String fileName) {
        try(PrintWriter out = new PrintWriter("feature_signatures/" + fileName + ".fs")  ) {
            for (Tuple tuple : fs.getTuples()) {
                Mat centroid = tuple.getCentroid();
                String line = "";
                for(int i = 0; i < 128; i++) {
                    if(i != 0) {
                        line += ",";
                    }
                    line += centroid.get(0, i)[0];
                }
                line += ":" + tuple.getWeight();
                out.println(line);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FeatureSignatureDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
