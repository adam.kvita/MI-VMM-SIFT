# Image retrieval na základě SIFT deskriptorů

Projekt vznikl v rámci semestrální práce v předmětu MI-VMM. Jeho jediným autorem je Adam Kvita.

## Obsah repozitáře

* [soubor project_report.pdf](project_report.pdf) - Zpráva k vypracování projektu
* [složka MI-VMM-SIFT](/MI-VMM-SIFT/) - Kompletní NetBeans projekt Java aplikace na výpočet podobnosti obrázků
* [složka web](/web/) - Nette projekt webové aplikace poskytující uživatelské rozhraní
* [složka computed_values](/computed_values/) - Hodnoty vypočtené v rámci experimentů s aplikací