package mi.vmm.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class FeatureSignature {
    private List<Tuple> tuples;

    public FeatureSignature() {
        tuples = new ArrayList<>();
    }
    
    public void addTuple(Tuple t) {
        tuples.add(t);
    }

    public List<Tuple> getTuples() {
        return tuples;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeatureSignature other = (FeatureSignature) obj;  
        if(this.tuples.size() != other.tuples.size()) {
            return false;
        } 
        for (int i = 0; i < this.tuples.size(); i++) {
            if(!this.tuples.get(i).equals(other.tuples.get(i))) {
                return false;
            }
        }
        return true;
    }
}
