package mi.vmm.utils;

import org.opencv.core.Core;
import org.opencv.core.Mat;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class MatUtils {
    public static boolean matsAreEqual(Mat mat1, Mat mat2){
        if (mat1.empty() && mat2.empty()) {
            return true;
        }
        if (mat1.cols() != mat2.cols() || mat1.rows() != mat2.rows() || mat1.dims() != mat2.dims()) {
            return false;
        }  
        for (int i = 0; i < mat1.rows(); i++) {
            for (int j = 0; j < mat1.cols(); j++) {
                if(mat1.get(i, j)[0] != mat2.get(i, j)[0]) {
                    return false;
                }
            }
        }
        return true;
    }
}
