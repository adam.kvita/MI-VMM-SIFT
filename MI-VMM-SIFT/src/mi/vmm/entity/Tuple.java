/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mi.vmm.entity;

import java.util.Objects;
import org.opencv.core.Mat;
import mi.vmm.utils.MatUtils;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class Tuple {
    private Mat centroid;
    private double weight;

    public Tuple(Mat centroid, double weight) {
        this.centroid = centroid;
        this.weight = weight;
    }

    public Mat getCentroid() {
        return centroid;
    }

    public void setCentroid(Mat centroid) {
        this.centroid = centroid;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuple other = (Tuple) obj;
        if (this.weight != other.weight) {
            return false;
        }       
        if(!MatUtils.matsAreEqual(this.centroid, other.centroid)) {
            return false;
        }
        return true;
    }
}
