package mi.vmm.SIFT;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import mi.vmm.dao.FeatureSignatureDAO;
import mi.vmm.dao.SimilarityMatrixDAO;
import mi.vmm.entity.Match;
import org.opencv.core.Core;

/**
 *
 * @author adam
 */
public class MIVMMSIFT {
    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

    public static void main(String[] args) {
        if(args.length > 0 && args.length < 3) {
            // set max descriptors
            if(args.length == 2) {
                FeatureSignatureDAO.setMaxKeypoints(Integer.parseInt(args[1]));
            }
            
            // generate whole similarity matrix if file does not exist
            File f = new File("similarity_matrix.db");
            if(!f.exists()) {
                generateSimilarityMatrix();
            }
            
            // find matches
            findMatches(args[0]);
        }
        else {
            System.out.println("Usage: imageName maxDescriptors");
        }
    }
    
    private static void findMatches(String needle) {
        ArrayList<Match> matches = new ArrayList<>();
        
        System.out.println("Finding images similar to " + needle);
        File folder = new File("img/");
        File[] files = folder.listFiles();
        for (File file : files) {
            String fileName = file.getName();
            if(!fileName.equals(needle)) {
                Image image;
                try {
                    image = ImageIO.read(file);
                    if (image != null) {
                        matches.add(new Match(
                            fileName, 
                            SimilarityMatrixDAO.getSimilarity(needle, fileName)
                        ));
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MIVMMSIFT.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        // sort images by distance asc
        Collections.sort(matches, new Comparator<Match>() {
            @Override
            public int compare(Match m1, Match m2) {
                if(m2.getSimilarity() < m1.getSimilarity()) {
                    return 1;
                }
                else if (m2.getSimilarity() > m1.getSimilarity()) {
                    return -1;
                }
                return 0;
            }
        });
        
        for (Match match : matches) {
            System.out.println("m:"+match.getFileName()+":" + match.getSimilarity());
        }
    }
    
    private static void generateSimilarityMatrix() {
        File folder = new File("img/");
        File[] files = folder.listFiles();
        int i = files.length * files.length;
        for (File first : files) {
            for (File second : files) {
                SimilarityMatrixDAO.getSimilarity(first.getName(), second.getName());
                Logger.getLogger("SimilarityMatrixGenerator").log(Level.INFO, "Generating similarity matrix, remaining cells: " + i--);
            }          
        }
    }
}
