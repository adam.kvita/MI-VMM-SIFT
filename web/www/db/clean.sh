#!/usr/bin/env bash
clear
echo "Cleaning generated files...";
rm -f feature_signatures/*;
rm -f keypoints/*;
rm -f log.log;
rm -f similarity_matrix.db; 
echo "Done.";