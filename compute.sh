#!/usr/bin/env bash
clear
IMG="balloon";
arr=("10" "20" "50" "100" "200" "300" "500");
for i in "${arr[@]}"
do
	echo "Compute similarities, n=$i";
	cd ./web/www/db/;
	time (java -jar -Djava.library.path="../../../MI-VMM-SIFT/lib/OpenCV/native/" ../../../MI-VMM-SIFT/dist/MI-VMM-SIFT.jar $IMG $i &> log.log) &> time.log;
	cd ../../../;
	mkdir -p ./computed_values/$i/feature_signatures/;
	mkdir -p ./computed_values/$i/keypoints/;
	mv ./web/www/db/feature_signatures/* ./computed_values/$i/feature_signatures/;
	mv ./web/www/db/keypoints/* ./computed_values/$i/keypoints/;
	mv ./web/www/db/similarity_matrix.db ./computed_values/$i/similarity_matrix.db;
	mv ./web/www/db/log.log ./computed_values/$i/log.log;
	mv ./web/www/db/time.log ./computed_values/$i/time.log;
done