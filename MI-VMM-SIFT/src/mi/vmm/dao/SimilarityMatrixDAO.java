package mi.vmm.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mi.vmm.entity.SimilarityMatrixCell;
import mi.vmm.utils.SQFDCalculator;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class SimilarityMatrixDAO {
    private static ArrayList<SimilarityMatrixCell> cells;
    
    static {
        cells = new ArrayList<>();
        loadFromFile();
    }
    
    private static void loadFromFile() {
        File f = new File("similarity_matrix.db");
        if(f.isFile()) { 
            try(BufferedReader in = new BufferedReader(new FileReader(f))) {
                String line = "";
                while((line = in.readLine()) != null) {
                    String[] pieces = line.split(":");
                    cells.add(new SimilarityMatrixCell(pieces[0], pieces[1], Double.parseDouble(pieces[2])));
                }                
            } catch (Exception ex) {
                Logger.getLogger(SimilarityMatrixDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private static void saveToFile() {
        try(PrintWriter out = new PrintWriter("similarity_matrix.db")  ) {
            for (SimilarityMatrixCell cell : cells) {
                out.println(cell.toString());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SimilarityMatrixDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void addCellToFile(SimilarityMatrixCell cell) {
        try(PrintWriter out = new PrintWriter(
                new FileOutputStream(
                    new File("similarity_matrix.db"), 
                    true
                )
            )
        ) {    
            out.println(cell.toString());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SimilarityMatrixDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static double getSimilarity(String firstImg, String secondImg) {
        for(SimilarityMatrixCell cell : cells) {
            if(cell.isBetween(firstImg, secondImg)) {
                return cell.getSimilarity();
            }
        }
        double similarity = SQFDCalculator.calculateSimilarity(
            FeatureSignatureDAO.getFeatureSignature(firstImg), 
            FeatureSignatureDAO.getFeatureSignature(secondImg)
        );  
        SimilarityMatrixCell cell = new SimilarityMatrixCell(
            firstImg, 
            secondImg,
            similarity 
        );
        cells.add(cell);
        addCellToFile(cell);
        return cell.getSimilarity();
    } 
}
