package mi.vmm.utils;

import java.util.List;
import mi.vmm.entity.FeatureSignature;
import mi.vmm.entity.Tuple;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class SQFDCalculator {
    public static double calculateSimilarity(FeatureSignature fs1, FeatureSignature fs2) {        
        List<Tuple> tuples1 = fs1.getTuples();
        List<Tuple> tuples2 = fs2.getTuples();
        
        Mat W = new Mat(1, tuples1.size() + tuples2.size(), CvType.CV_64F);
        for (int i = 0; i < W.cols(); i++) {
            if(i < tuples1.size()) {
                W.put(0, i, tuples1.get(i).getWeight());
            } 
            else {
                W.put(0, i, -tuples2.get(i - tuples1.size()).getWeight());
            }
        }
        
        Mat A = new Mat(tuples1.size() + tuples2.size(), tuples1.size() + tuples2.size(), CvType.CV_64F);
        for (int i = 0; i < A.rows(); i++) {
            for (int j = 0; j < A.cols(); j++) {
                Mat centroid1 = null;
                Mat centroid2 = null;                        
                if(i < tuples1.size()) {
                    centroid1 = tuples1.get(i).getCentroid();
                } 
                else {
                    centroid1 = tuples2.get(i - tuples1.size()).getCentroid();
                }
                if(j < tuples1.size()) {
                    centroid2 = tuples1.get(j).getCentroid();
                }
                else {
                    centroid2 = tuples2.get(j - tuples1.size()).getCentroid();
                }
                A.put(i, j, calculateCentroidSimilarity(centroid1, centroid2));
            }
        }
        
        Mat WA = new Mat(1, W.cols(), CvType.CV_64F); 
        Core.gemm(W, A, 1, new Mat(), 0, WA);
        
        Mat WAWt = new Mat(1, 1, CvType.CV_64F);
        Core.gemm(WA, W.t(), 1, new Mat(), 0, WAWt);
        
        return Math.sqrt(WAWt.get(0, 0)[0]);
    }
    
    private static double calculateCentroidSimilarity(Mat centroid1, Mat centroid2) {
        double distance = calculateCentroidDistance(centroid1, centroid2);
        return 1.0 / (1.0 + distance);
    }
    
    private static double calculateCentroidDistance(Mat centroid1, Mat centroid2) {
        double number = 0;
        for (int i = 0; i < 128; i++) {
            number += Math.pow(centroid1.get(0, i)[0] - centroid2.get(0, i)[0], 2);
        }
        return Math.sqrt(number);
    }
}
