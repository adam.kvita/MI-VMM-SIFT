<?php

namespace App\Presenters;

use app\components\UploadControl\IUploadControlFactory;
use Nette;
use app\components\UploadControl\UploadControl;
use Nette\DI\Container;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var UploadControl @inject */
    public $upload;

    /** @var Container @inject */
    public $container;

    public function actionDefault($id) {
        $dbDir = $this->container->getParameters()["dbDir"];
        $siftCalculator = $this->container->getParameters()["siftCalculator"];
        $openCVNativeLibrary = $this->container->getParameters()["openCVNativeLibrary"];
        $template = $this->template;
        $images = array();
        if ($id != "" && @getimagesize($dbDir . "img/" . $id) != false) {
            $template->curImg = [
                "id" => $id,
                "imgPath" => "./db/img/" . $id,
                "keypointsPath" => "./db/keypoints/" . $id . ".jpg"
            ];
            $command = "(cd " . $dbDir . " && exec java -jar -Djava.library.path=\"" . $openCVNativeLibrary . "\" " . $siftCalculator . " " . $id . " 2> log.log)";
            exec($command, $output);
            foreach ($output as $image) {
                $pieces = explode(":", $image);
                if ($pieces[0] === "m") {
                    $images[] = [
                        "id" => $pieces[1],
                        "imgPath" => "./db/img/" . $pieces[1],
                        "keypointsPath" => "./db/keypoints/" . $pieces[1] . ".jpg",
                        "SQFD" => round($pieces[2], 4)
                    ];
                }
            }
        }
        else {
            $files = scandir($dbDir . "img/");
            foreach ($files as $file) {
                if (is_file($dbDir . "img/" . $file) && @getimagesize($dbDir . "img/" . $file) != false) {
                    $images[] = [
                        "id" => $file,
                        "imgPath" => "./db/img/" . $file,
                        "keypointsPath" => "./db/keypoints/" . $file . ".jpg"
                    ];
                }
            }
        }
        $template->images = $images;
    }

    public function createComponentUpload() {
        $this->upload->onFileUpload[] = function($fileName) {
            $this->redirect('this', ['id' => $fileName]);
        };
        return $this->upload;
    }
}
