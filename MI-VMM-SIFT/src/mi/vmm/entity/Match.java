package mi.vmm.entity;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class Match {
    private String fileName;
    private double similarity;

    public Match(String fileName, double similarity) {
        this.fileName = fileName;
        this.similarity = similarity;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(double similarity) {
        this.similarity = similarity;
    }
}
