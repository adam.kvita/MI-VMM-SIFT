<?php

namespace app\components\UploadControl;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;


class UploadControl extends Control
{
    private $uploadDir;
    public $onFileUpload;

    public function __construct($uploadDir)
    {
        parent::__construct();
        $this->uploadDir = $uploadDir;
    }

    public function createComponentUpload()
    {
        $form = new Form;
        $form->addUpload('image', 'Image')
            ->setRequired(true)
            ->addRule(Form::IMAGE, 'Only JPEG, PNG and GIF supported.')
            ->addRule(Form::MAX_FILE_SIZE, 'Maximal image size is 2 MB.', 2 * 1024 * 1024)
            ->getLabelPrototype()->setAttribute('class', 'hidden');
        $form->addSubmit('upload', 'Upload')
            ->setAttribute('class', 'pure-button pure-button-upload');
        $form->onSuccess[] = [$this, 'processForm'];
        $form->elementPrototype->addAttributes(array('class' => 'pure-form upload-form'));
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = "";
        $renderer->wrappers['pair']['container'] = "";
        $renderer->wrappers['label']['container'] = "";
        $renderer->wrappers['control']['container'] = "";
        return $form;
    }

    /**
     * @param  Form $form
     * @return void
     */
    public function processForm($form)
    {
        $values = $form->getValues();
        $file = $values["image"];
        if($file->isOK()) {
            $fileName = sha1(date('c'));
            $destination = $this->uploadDir . $fileName;
            $file->move($destination);
            if($file->getContentType() == "image/gif") {
                imagepng(imagecreatefromstring(file_get_contents($destination)), $destination);
            }
            $this->onFileUpload($fileName);
        }
    }

    public function render() {
        $template = $this->template;
        $template->setFile(__DIR__ . '/UploadControl.latte');
        $template->render();
    }
}