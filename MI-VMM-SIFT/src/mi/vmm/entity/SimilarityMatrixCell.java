package mi.vmm.entity;

/**
 *
 * @author Adam Kvita <kvitaada@fit.cvut.cz>
 */
public class SimilarityMatrixCell {
    private String firstImg;
    private String secondImg;
    private double similarity;

    public SimilarityMatrixCell(String firstImg, String secondImg, double similarity) {
        this.firstImg = firstImg;
        this.secondImg = secondImg;
        this.similarity = similarity;
    }
    
    public boolean isBetween(String imgName1, String imgName2) {
        if((firstImg.equals(imgName1) && secondImg.equals(imgName2))
            || (firstImg.equals(imgName2) && secondImg.equals(imgName1))) {
            return true; 
        }
        return false;
    }
    
    public double getSimilarity() {
        return similarity;
    }

    @Override
    public String toString() {
        return firstImg + ":" + secondImg + ":" + similarity;
    }
}
